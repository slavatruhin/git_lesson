#include <iostream>

void f(int);
int sqrt(int);

int main()
{
    std::cout << "Hi!" << std::endl;
    int a = 0;
    a += 7;
	a = sqrt(a);
    f(a);
    return 0;
}

void f(int a)
{
    std::cout << "a = " << a << "\n";
}

int sqrt(int in)
{
	return in*in;
}